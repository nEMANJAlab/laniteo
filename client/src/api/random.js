import axios from "axios";

export const fetchNumbers = async (total, min, max) => {
    return await axios.post("https://api.random.org/json-rpc/1/invoke", {
        "jsonrpc": "2.0",
        "method": "generateIntegers",
        "params": {
            "apiKey": "c1e3b8d3-cd49-4c20-90f2-25810b5e4c48",
            "n": `${total}`,
            "min": `${min}`,
            "max": `${max}`,
            "replacement": true
        },
        "id": 42
    })
}