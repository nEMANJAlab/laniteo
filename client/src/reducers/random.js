import { FETCH_NUMBERS, FETCH_NUMBERS_SUCCESS, FETCH_NUMBERS_FAILURE } from '../actions/random';

export default (state = {list: [], created: false, loading: false, error: false}, action) => {
    switch(action.type) {
        case FETCH_NUMBERS:
            return { ...state }
        case FETCH_NUMBERS_SUCCESS:
            return {
                ...state,
                list: action.payload
            }
        case FETCH_NUMBERS_FAILURE:
            return { ...state, error: true}
        default:
            return state;
    }
}