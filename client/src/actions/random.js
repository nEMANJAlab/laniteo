export const FETCH_NUMBERS = "random/FETCH_NUMBERS";
export const FETCH_NUMBERS_SUCCESS = "random/FETCH_NUMBERS_SUCCESS";
export const FETCH_NUMBERS_FAILURE = "random/FETCH_NUMBERS_FAILURE";

export const fetchNumbers = (total, min, max) => ({
    type: FETCH_NUMBERS,
    payload: {total, min, max}
})
