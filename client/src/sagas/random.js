import { FETCH_NUMBERS, FETCH_NUMBERS_SUCCESS, FETCH_NUMBERS_FAILURE } from '../actions/random';
import { call, put, takeLatest } from "redux-saga/effects";

import { fetchNumbers as fn } from '../api/random';

function* fetchNumbers(action) {
    try{
        const data = yield call(fn, action.payload.total, action.payload.min, action.payload.max);
        yield put({ type: FETCH_NUMBERS_SUCCESS, payload: data.data.result.random.data})
    }catch(e){
        yield put({ type: FETCH_NUMBERS_FAILURE});
    }
}

export default function* randomSaga() {
    yield takeLatest(FETCH_NUMBERS, fetchNumbers);
}