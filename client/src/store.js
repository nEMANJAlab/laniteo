import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import logger from 'redux-logger'
import randomReducer from './reducers/random'
import randomSaga from './sagas/random'

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

middlewares.push(logger);

const composedEnhancers = compose(
    applyMiddleware(...middlewares)
);

export default createStore(randomReducer, composedEnhancers);

sagaMiddleware.run(randomSaga);