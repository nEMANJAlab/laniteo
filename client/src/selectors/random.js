import { createSelector } from 'reselect'

const numbersSelector = state => state.list

const graphDataSelector = createSelector(
    numbersSelector,
  numbers => returnCounts(numbers)
)

const returnCounts = (props) =>{
    let i;
    var a, count = {}, arr = props;

    for (i = 0; i < arr.length; i++) {
        a = arr[i];
        count[a] = (count[a] || 0) + 1;
    }
    const graphData = {
        nums: Object.keys(count),
        occurances: Object.values(count),
        numbers: props
    }

    return graphData;
}

export default graphDataSelector;