import React from 'react';
import { Navbar, NavbarBrand } from 'mdbreact';
import { BrowserRouter as Router} from 'react-router-dom';
import Logo from '../laniteo.png'

class NavbarJS extends React.Component {

    render() {
        return (
            <Router>
                <div>
                <Navbar color="special-color" dark expand="md" scrolling>
                    <NavbarBrand href="/">
                        <img src={Logo} alt="laniteoLogo"/>
                    </NavbarBrand>
                </Navbar>
                </div>
            </Router>
        );
    }
}

export default NavbarJS;