import React from 'react';
import { Container } from 'mdbreact';
import { Line } from 'react-chartjs-2';
import { connect } from 'react-redux';
import graphDataSelector from '../selectors/random';

class ChartsPage extends React.Component {

    render() {

        const { occurances, nums, numbers } = this.props.numbers; 
        const data = {
            labels: nums,
            datasets: [
              {
                label: 'Occurances',
                pointRadius: 6,
                pointHitRadius: 1,
                pointHoverBorderWidth: 2,
                pointHoverBorderColor: '#ffbb33',
                pointBackgroundColor: '#ff4444',
                borderCapStyle: 'butt',
                pointBorderWidth: 15,
                pointHoverRadius: 25,
                data: occurances
              }
            ]
          };
          
        return (
        <Container>
            <Line data={data}/>
            <span className="white-text" > Numbers from Random.org: [ 
            {
              numbers.map((number, id) => {
                return(
                <span key={id}>{number}, </span> 
                )
              })
            } ]</span>
        </Container>
        );
      }
};

const mapStateToProps = state => ({
    numbers: graphDataSelector(state)
})

export default connect(mapStateToProps)(ChartsPage);