import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchNumbers } from '../actions/random';
import { Container, Row, Col, Input, Button, Fa, Modal, ModalBody, ModalFooter } from 'mdbreact';

class Random extends Component {
    constructor(){
        super();

        this.state = {
            modal: false,
            numAmount:null,
            min: null,
            max: null
        }
        this.toggle = this.toggle.bind(this);
       
    }

    toggle() {
        this.setState({
          modal: !this.state.modal
        });
      }

    updateAmount = e => {
        this.setState({
            numAmount: e.target.value
        })
    }

    updateMin = e =>{
        this.setState({
            min: e.target.value
        })
    }
    updateMax = e =>{
        this.setState({
            max: e.target.value
        })
    }

    render() {
        return (
        <div>
            <br/>
            <Container>
                <Row>
                <Col md="12">
                    <Button color="info" onClick={this.toggle} >Enter details</Button>
                    <Modal isOpen={this.state.modal} toggle={this.toggle} className="cascading-modal">
                    <div className="modal-header primary-color white-text">
                        <h4 className="title">
                        <Fa className="fa fa-pencil" /> Random number generator</h4>
                        <button type="button" className="close" onClick={this.toggle}>
                        <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <ModalBody className="grey-text">
                        <Input onChange={this.updateAmount} size="sm" label="How many integers you need?" group type="number" validate error="wrong" success="right"/>
                        <Input onChange={this.updateMin} size="sm" label="The lower boundary for the range:" group type="number" validate error="wrong" success="right"/>
                        <Input onChange={this.updateMax} size="sm" label="The upper boundary for the range " group type="number" validate error="wrong" success="right"/>
                    </ModalBody>
                    <ModalFooter>
                        <Button color="secondary" onClick={this.toggle}>Close</Button>{' '}
                        <Button color="primary" onClick={()=>{ this.props.fetchNumbers(this.state.numAmount, this.state.min, this.state.max); this.toggle() }}>Done</Button>
                    </ModalFooter>
                    </Modal>
                </Col>
                
                </Row>
            </Container>
        </div>
        )
    }
}

const mapStateToProps = state => ({
   numbers: state.list.data
})

const mapDispatchToProps = dispatch => bindActionCreators({ fetchNumbers }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Random);