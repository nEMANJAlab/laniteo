import React, { Component } from 'react';
import store from './store';
import './App.css';
import {Provider} from "react-redux";
import Random from './components/Random';
import Navbar from './components/Navbar';
import Graph from './components/Graph';

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Navbar/>
          <br/>
          <Graph/>
         <Random/> 
        </div>
      </Provider>
    );
  }
}

export default App;
